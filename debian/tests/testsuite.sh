#!/bin/sh

set -e

# prepare test env
export PATH=$PATH:/usr/lib/pacemaker
export PYTHONPATH=/usr/share/crmsh
# test/unittests/test_report.py::test_ts_to_dt assumes UTC
export TZ=UTC

mkdir /usr/share/crmsh/doc

# some tests fail if the cluster is running
service pacemaker stop
service corosync stop

cd /usr/share/crmsh/tests/unittests
printf "Running unittests...\n"
py.test-3 -vv

cp /usr/share/pacemaker/*.rng /usr/share/crmsh/tests/unittests/schemas/

cd /usr/share/crmsh/tests
printf "\nRunning cibtests...\n"
./cib-tests.sh

printf "\nRunning regressions...\n"
./regression.sh -m buildbot || true
cat crmtestout/regression.out
